<?php

namespace AppBundle\Entity\Traits;

use Symfony\Component\Security\Core\User\UserInterface;

trait AuthorAble
{

    /**
     * Author of the comment
     *
     * @ORM\ManyToOne(targetEntity="AppBundleEntity\User")
     * @var UserInterface
     */
    protected $author;

    public function setAuthor(UserInterface $author)
    {
        $this->author = $author;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function getAuthorName()
    {
        if (null === $this->getAuthor()) {
            return 'Anonymous';
        }

        return $this->getAuthor()->getUsername();
    }
}