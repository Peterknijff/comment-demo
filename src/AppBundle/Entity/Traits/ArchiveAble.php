<?php

namespace AppBundle\Entity\Traits;

trait ArchiveAble
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean", nullable=false)
     *
     */
    private $archived;

    /**
     * Is archived
     *
     * @return string
     */
    public function isArchived()
    {
        return $this->archived;
    }
    /**
     * Set archived
     *
     * @param boolean $boolean
     * @return $this
     */
    public function setArchived($boolean)
    {
        $this->archived = (Boolean) $boolean;
        return $this;
    }
}