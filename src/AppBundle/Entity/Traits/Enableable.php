<?php

namespace AppBundle\Entity\Traits;

trait Enableable
{
    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false)
     *
     */
    private $enabled;

    /**
     * Is enabled
     *
     * @return string
     */
    public function isEnabled()
    {
        return $this->enabled;
    }
    /**
     * Set Enabled
     *
     * @param boolean $boolean
     * @return $this
     */
    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean) $boolean;
        return $this;
    }
}