<?php

namespace AppBundle\Entity\Traits;

trait DateTimeAble {

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime")
     */
    private $datetime;

    /**
     * Set date
     *
     * @param \DateTime $datetime
     *
     * @return $this
     */
    public function setDateTime($datetime)
    {
        $this->datetime = $datetime;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDateTime()
    {
        return $this->datetime;
    }

}