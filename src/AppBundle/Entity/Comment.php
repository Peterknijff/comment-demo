<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Comment as BaseComment;
// GEDMO IS USED HERE BY TRAITS
use FOS\CommentBundle\Model\SignedCommentInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use AppBundle\Entity\Traits as Traits;

/**
 * @ORM\Entity
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class Comment extends BaseComment
{
    use Traits\TimeStampable,
        Traits\TitleAble,
        Traits\Enableable;

    // Construction method, override parent and add data
    public function __construct()
    {
        parent::__construct();
        $this->setEnabled(true);
    }

    /**
     * @return string name of the comment author
     */
    public function getAuthorName()
    {
        return $this->getTitle();
    }


    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type = "integer")
     * @ORM\GeneratedValue(strategy = "IDENTITY")
     */
    protected $id;

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Thread of this comment
     *
     * @var Thread
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Thread")
     */
    protected $thread;
}
