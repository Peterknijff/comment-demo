<?php

namespace AppBundle\Form;

use AppBundle\Entity\Comment;
use AppBundle\Event\CommentFormSubscriber;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * commentType
 * Changing original form
 */
class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->remove('body');
        $builder->add('title',  'Symfony\Component\Form\Extension\Core\Type\TextType', [
            'label' => 'Naam',
            'empty_data' => 'Naam',
        ]);
        $builder->add('body', 'Symfony\Component\Form\Extension\Core\Type\TextareaType', [
            'label' => 'Bericht',
            'empty_data' => 'Bericht'
        ]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

        });
    }
    public function setDefaultOptions(OptionsResolver $resolver)
    {
        // Set dataclass
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Comment',
        ));
    }

    public function getParent()
    {
        // Override Class
        return 'FOS\CommentBundle\Form\CommentType';
    }

    public function getName()
    {
        // Name of the formtype
        return "app_comment";
    }
}