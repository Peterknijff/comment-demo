<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Thread;
use Doctrine\ORM\ORMException;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * indexAction
     * Shows all enabled threads, provides a submit button to add new thread on the fly (testing purposes)
     *
     * @param Request $request HTTP Request
     * @return Response A Response instance
     */
    public function indexAction(Request $request)
    {
        // Get Entitymanager and Thread repository
        $em = $this->getDoctrine()->getManager();
        $threadRepo = $em->getRepository('AppBundle:Thread');

        // Get all threads
        $threads = $threadRepo->findBy(['enabled' => 1], ['createdAt' => 'DESC']);

        // New Thread object (for submitting a new one via form)
        $thread = new Thread();

        // Simple form generation
        // TODO: Internationalization for label
        $form = $this->createFormBuilder($thread)
            ->add('save', SubmitType::class, array('label' => 'Toevoegen', 'attr' => ['class' => 'btn btn-default pull-right']))
            ->getForm();

        $form->handleRequest($request);

        // Check if submit button is pressed and is valid
        if ($form->isSubmitted() && $form->isValid()) {

            // Create a faker object (in order to generate random data)
            $faker = Factory::create();

            // add new Thread data and add it to the database
            $thread->setTitle(str_replace('.', '', $faker->realText(25)));
            $thread->setPermalink($faker->title);
            $thread->setEnabled(true);

            try {
                $em->persist($thread);
                $em->flush();
            }catch(ORMException $e){
                // TODO: Check for errors, maybe service for checking what the problem causes
                $this->addFlash('error', 'There was a problem inserting a new Thread to the database.');
                return $this->redirect($request->headers->get('referer'));

            }catch(Exception $e){
                // TODO: Check for errors, maybe service for checking what the problem causes
                $this->addFlash('error', 'There was a problem.');
                return $this->redirect($request->headers->get('referer'));
            }
            // If all went well, return to the new generated Thread
            return $this->redirectToRoute('thread', ['thread_id' => $thread->getId()]);
        }

        // Return to the correct view with Threads and form
        return $this->render('@App/index.html.twig',
            [
                'threads' => $threads,
                'form' => $form->createView(),
            ]
        );
    }

    /**
     * showThreadAction
     * Show detailed information about a thread,
     *
     * @param string $thread_id Parameter to identify thread
     * @param Request $request HTTP Request
     * @return Response A Response instance
     */
    public function showThreadAction($thread_id, Request $request)
    {
        // Get specific Thread
        $thread = $this->container->get('fos_comment.manager.thread')->findThreadById($thread_id);

        // If Thread does not exist, return to index with status: Moved Permanently
        if(empty($thread)){
            return $this->redirectToRoute('index', [], 301);
        }

        // Get commenttree via Thread
        $comments = $this->container->get('fos_comment.manager.comment')->findCommentTreeByThread($thread);

        // Return to the correct view with thread and commenttree
        return $this->render('@App/thread.html.twig',
            [
                'thread' => $thread,
                'comments' => $comments,
            ]
        );
    }

}
