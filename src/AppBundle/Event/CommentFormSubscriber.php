<?php

namespace AppBundle\Event;

use AppBundle\Entity\Comment;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;

class CommentFormSubscriber implements EventSubscriberInterface
{
    public function form(FormEvent $event)
    {
        // Form manipulation
        $data = $event->getData();

        return $event->setData($data);

    }
    public static function getSubscribedEvents()
    {
        return array(
            FormEvents::PRE_SET_DATA => array('form')
        );
    }
}