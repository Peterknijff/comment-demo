<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Thread;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadThreats extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @annotation
     * @var $container ContainerInterface
     */
    private $container;

    /**
     * @annotation
     * @var $manager ObjectManager
     */
    private $manager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // Get the entity manager
        $this->manager = $manager;

        // Threads dummy data
        $threads = array(
            array(
                'title' => 'Reageer functionaliteit',
            ),
            array(
                'title' => 'Nieuwe collega',
            ),
            array(
                'title' => 'Beurs!',
            ),
        );

        // Insert in database
        $this->newThreads($threads);
        $manager->flush();
    }

    /**
     * newThreads, Function to persist the data
     * @param array $threads array with threads
     */
    private function newThreads($threads)
    {
        // Loop over arrays, create Thread Object and set data where needed
        foreach($threads as $item){
            $thread = new Thread();
            $thread
                ->setTitle($item['title'])
                ->setEnabled(true)
                ->setPermalink('permalink')
            ;
            $thread->setNumComments(3);
            $thread->setLastCommentAt(new \DateTime('NOW'));
            $this->manager->persist($thread);
        }
        return;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 0; // the order in which fixtures will be loaded
    }
}