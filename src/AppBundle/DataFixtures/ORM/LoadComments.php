<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Comment;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * loadComments
 * OrderedFixtureInterface for dummy data
 */
class LoadComments extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @annotation
     * @var $container ContainerInterface
     */
    private $container;

    /**
     * @annotation
     * @var $manager ObjectManager
     */
    private $manager;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * load, Function to load dummy data into database
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Get the entity manager and find all existing Threads
        $this->manager = $manager;
        $threads = $manager->getRepository('AppBundle:Thread')->findAll();

        // Comments with comments on comment, dummy data
        $comments = array(
            array(
                'title' => 'Peter Knijff',
                'comments' => [
                    'Marnix Spambot',
                    'Gerard van Westen',
                    'Jan Heshof'
                ],
            ),
            array(
                'title' => 'Frits de Vries',
            ),
            array(
                'title' => 'Gerben Wieringa',
            ),
        );

        // Insert in database
        $this->newComments($threads, $comments);
        $manager->flush();
    }

    /**
     * newComments, Function to persist the data
     * @param array $threads array with threads
     * @param array $comments array with comments
     */
    private function newComments($threads, $comments)
    {
        // Loop over arrays, create Comment Object and set data where needed
        $commentText = 'Dit is het % bericht op: ';
        $commentOnCommentText = 'Dit is het % bericht als reactie op: ';
        for($i = 0; count($threads) > $i; $i++){
            foreach($comments as $key=>$comment){
                $commentObject = new Comment();
                $commentObject
                    ->setTitle($comment['title'])
                    ->setEnabled(true)
                    ->setBody(str_replace('%', ($key + 1).'e', $commentText. $threads[$i]->getTitle()));
                $commentObject->setThread($threads[$i]);
                $this->manager->persist($commentObject);

                if(isset($comment['comments'])){
                    $this->manager->flush();
                    foreach($comment['comments'] as $key2=>$ancestor){
                        $commentOnComment = new Comment();
                        $commentOnComment
                            ->setTitle($ancestor)
                            ->setEnabled(true)
                            ->setBody(str_replace('%', ($key2 + 1).'e', $commentOnCommentText. $commentObject->getTitle()));
                        $commentOnComment->setThread($threads[$i]);
                        $commentOnComment->setParent($commentObject);
                        $this->manager->persist($commentOnComment);
                    }
                }
            }
        }
        return;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}