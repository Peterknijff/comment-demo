Backend developer
=================

**Opdracht:** 
De website van één van onze opdrachtgevers bevat nieuwsberichten. De opdrachtgever wil dat mensen kunnen reageren op die nieuwsberichten. Om dat te kunnen doen, wordt er besloten om onder iedere nieuwsbericht een reactiemogelijkheid op te nemen.

Installatie
--------------
### A) Project clonen en installeren
  1. Clone "git@bitbucket.org:Peterknijff/comment-demo.git" vanuit de GIT repository naar een lokale ontwikkelomgeving
  2. Ga via de terminal naar de folder waar het project in gecloned is
  3. In de root van het project, installeer alle dependencies door: ```$composer install``` uit te voeren.
  4. Zorg dat in **app/config/parameters.yml** de juiste credentials voor een databaseconnectie ingevuld staan
  
### B) Database
  1. De database moet aangemaakt worden, dit kan via de terminal door het volgende uit te voeren: 
        2. Database aanmaken: ```$php bin/console doctrine:database:create```
        2. Schema aanaken: ```$php bin/console doctrine:schema:create```
  2.  Optioneel is de database vullen met demo-data: ```$php bin/console doctrine:fixtures:load```, de database wordt geleegd en opnieuw gevuld, voer een ```y``` een indien akkoord.

### C) Assets
  1. Bootstrap font-awesome installeren: ```$php bin/console braincrafted:bootstrap:install```
  1. CSS en JS bestanden installeren: ```$php bin/console assetic:dump```
  
### D) Applicatie bereiken
  1. De applicatie is nu te bereiken via de browser (afhankelijk van host instellingen)
  1. Als alternatief kan de interne webserver gestart worden, hierdoor wordt de app toegankelijk via het lokale IP: ```$php bin/console server:run```
  
  Enkele vereisten:
  
  * **PHP** - PHP moet geïnstalleerd zijn en uitvoerbaar vanuit de terminal
  * [**Composer**][1] - Composer is nodig om alle dependencies te kunnen installeren (PHP Dependency manager )

Implementatie
--------------
Een website gemaakt met Symfony kan deze implementatie van reacties overnemen op elke willekeurige entiteit. Of dit nu een project is, een product of een nieuwsitem.

Deze applicatie maakt reeds gebruik van een externe bundel die een aantal functionaliteiten voorziet. Deze functionaliteiten kunnen verder worden uitgebreid en worden aangepast, zoals in deze applicatie gebeurd is.

Echter is het verstandig om nog iets in te bouwen tegen Spam, bijvoorbeeld een CAPTCHA mogelijkheid. FOSUserBundle zou een andere optie zijn om alleen ingelogde gebruikers te kunnen laten reageren.

Beslissingen
--------------
Het gebruiken van een bundel neemt voordelen en nadelen met zich mee, maar toch vaak kies ik er voor om naar een bundel te kijken die een bepaalde functionaliteit reeds voorziet. Zo heb ik gekozen voor de [**FOSCommentBundle**][2].

Deze bundel wordt sinds 2011 ontwikkeld door een groot aantal developers, het voordeel van deze keuze is dat zij in al die jaren reeds erg veel energie en moeite hebben gestoken in het optimaliseren en ontwikkelen van deze bundel. Daarnaast is het zo dat er vrij snel en eenvoudig een werkbare functionaliteit toegoegd kan worden aan iedere applicatie.

Zo ook aan dit project, ik heb de instalatie procedure gevolgd zoals zij dit [**hier**][3] vermelden.
Eenmaal aan de gang merk je dat je vaak toch wel tegen dingen aanloopt die je moet oplossen.

1. De bundel heeft de functionaliteit om FOSUserBundle te integreren, echter omschrijft de opdracht daar niks over (inloggen vereist). Er moet dus een omweg gevonden worden om een naam veld toe te voegen.
2. Het oog wilt ook wat, zo heb ik views zodanig gemaakt dat reacties direct in de lijst tevoorschijn komen. Er kan tot 3 levels diep gereageerd worden.
3. Waar ik tegen aan liep was:
    1. Het overriden van het bestaande formulier en de controller
    2. Het werkend maken van de bundel in combinatie met de front-end
    3. Vanuit de bundel zat het toevoeg formulier direct in het berichten overzicht (div in een ul), deze heb ik los getrokken van elkaar en met javascript er voor gezorgd dat het toegevoegde item correct wordt toegevoegd aan de lijst.
4. Ik heb daarnaast nog een aantal uitbreidingen gemaakt op de opdracht: 
    1. Dummydata, er kan zodra de database en het schema is aangemaakt, d.m.v. datafixtures de database vullen
    2. De mogelijkheid om extra projecten toe te voegen door op toevoegen te drukken in het overzicht (voor het gemak)

[1]:  https://getcomposer.org/download/
[2]:  https://github.com/FriendsOfSymfony/FOSCommentBundle
[3]:  https://github.com/FriendsOfSymfony/FOSCommentBundle/blob/master/Resources/doc/index.md
